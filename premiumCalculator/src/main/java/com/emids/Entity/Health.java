package com.emids.Entity;

public class Health {

	/**
	 * Checking hiperTension of the person.
	 */
	private Boolean hiperTension;
	/**
	 * Checking bloodPressure of the person.
	 */
	private Boolean bloodPressure;
	/**
	 * Checking bloodSugar of the person.
	 */
	private Boolean bloodSugar;
	/**
	 * Checking overWeight of the person.
	 */
	private Boolean overWeight;

	/**
	 * Retrieves hiperTension of the person.
	 * 
	 * @return hiperTension.
	 */
	public Boolean getHiperTension() {
		if (null == hiperTension) {
			hiperTension = false;
		}
		return hiperTension;
	}

	/**
	 * Sets hiperTension of the person.
	 * 
	 * @param hiperTension
	 *            - hiperTension of the person.
	 */
	public void setHiperTension(Boolean hiperTension) {
		this.hiperTension = hiperTension;
	}

	/**
	 * Retrieves bloodPressure of the person.
	 * 
	 * @return bloodPressure.
	 */
	public Boolean getBloodPressure() {
		if (null == bloodPressure) {
			bloodPressure = false;
		}
		return bloodPressure;
	}

	/**
	 * Sets bloodPressure of the person.
	 * 
	 * @param bloodPressure
	 *            - bloodPressure of the person.
	 */
	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	/**
	 * Retrieves bloodSugar of the person.
	 * 
	 * @return bloodSugar
	 */
	public Boolean getBloodSugar() {

		if (null == bloodSugar) {
			bloodSugar = false;
		}
		return bloodSugar;
	}

	/**
	 * Sets bloodSugar of the person.
	 * 
	 * @param bloodSugar
	 *            - bloodSugar of the person.
	 */
	public void setBloodSugar(Boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	/**
	 * Retrieves overWeight of the person.
	 * 
	 * @return overweight.
	 */
	public Boolean getOverWeight() {

		if (null == overWeight) {
			overWeight = false;
		}
		return overWeight;
	}

	/**
	 * Sets overweight of the person.
	 * 
	 * @param overWeight
	 *            - overWeight of the person.
	 */
	public void setOverWeight(Boolean overWeight) {
		this.overWeight = overWeight;
	}

}
