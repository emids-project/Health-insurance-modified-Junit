package com.emids.EntityImplementation;

import com.emids.Entity.Person;

public class InsurancePremiumCalculator {

	/**
	 * This method calculates insurance premium for input person.
	 * @param person - Input person
	 * @return Insurance premium amount
	 */
	public double calculatePremiumPercentage(Person person) {

		double premiumAmount = premiumConstants.BASE_PREMIUM;

		if (person.getAge() > premiumConstants.AGE_18) {
			premiumAmount = calculateBasedOnAgeCriteria(person, premiumAmount);
			premiumAmount = calculateBasedOnGenderCriteria(person, premiumAmount);
			premiumAmount = calculateBasedOnHealthCondition(person, premiumAmount);
			premiumAmount = calculateBasedOnHabits(person, premiumAmount);
		} 

		return Math.round(premiumAmount);
	}

	/**
	 * 
	 * @param personObject
	 * @param premiumAmount
	 * @return
	 */
	public double calculateBasedOnAgeCriteria(Person personObject, double premiumAmount) {

		PremiumCalculator agePremiumCalculator = (person, criteria, premiumAmt) -> {

			if (criteria.equalsIgnoreCase(premiumConstants.CALCULATE_AGE)) {

				if (person.getAge() >= 18) {
					premiumAmt += premiumAmt * premiumConstants.AGE_18_TO_25 / 100;
				}

				if (person.getAge() >= 25) {
					premiumAmt += premiumAmt * premiumConstants.AGE_25_TO_30 / 100;
				}

				if (person.getAge() >= 30) {
					premiumAmt += premiumAmt * premiumConstants.AGE_30_TO_35 / 100;
				}

				if (person.getAge() >= 35) {
					premiumAmt += premiumAmt * premiumConstants.AGE_35_TO_40 / 100;
				}

				if (person.getAge() > 40) {
					premiumAmt += premiumAmt * premiumConstants.AGE_40_PLUS / 100;
				}
			}

			return premiumAmt;
		};

		return agePremiumCalculator.calculate(personObject, premiumConstants.CALCULATE_AGE, premiumAmount);
	}

	/**
	 * 
	 * @param personObject
	 * @param premiumAmount
	 * @return
	 */
	public double calculateBasedOnGenderCriteria(Person personObject, double premiumAmount) {

		PremiumCalculator genderPremiumCalculator = (person, criteria, premiumAmt) -> {

			if (criteria.equalsIgnoreCase(premiumConstants.CALCULATE_GENDER)) {
				if (premiumConstants.GENDER_MALE == person.getGender()) {
					premiumAmt += premiumAmt * premiumConstants.MALE_PREMIUM / 100;
				}
			}

			return premiumAmt;
		};

		return genderPremiumCalculator.calculate(personObject, premiumConstants.CALCULATE_GENDER, premiumAmount);
	}

	/**
	 * 
	 * @param personObject
	 * @param premiumAmount
	 * @return
	 */
	public double calculateBasedOnHealthCondition(Person personObject, double premiumAmount) {

		PremiumCalculator healthPremiumCalculator = (person, criteria, premiumAmt) -> {

			if (criteria.equalsIgnoreCase(premiumConstants.CALCULATE_BODY_CONDITION)) {

				if (person.getHealth().getHiperTension()) {
					premiumAmt += premiumAmt * premiumConstants.BODY_CONDITION_PREMIUM / 100;
				}

				if (person.getHealth().getBloodPressure()) {
					premiumAmt += premiumAmt * premiumConstants.BODY_CONDITION_PREMIUM / 100;
				}

				if (person.getHealth().getBloodSugar()) {
					premiumAmt += premiumAmt * premiumConstants.BODY_CONDITION_PREMIUM / 100;
				}

				if (person.getHealth().getOverWeight()) {
					premiumAmt += premiumAmt * premiumConstants.BODY_CONDITION_PREMIUM / 100;
				}

			}
			return premiumAmt;

		};

		return healthPremiumCalculator.calculate(personObject, premiumConstants.CALCULATE_BODY_CONDITION, premiumAmount);
	}

	/**
	 * 
	 * @param personObject
	 * @param premiumAmount
	 * @return
	 */
	public double calculateBasedOnHabits(Person personObject, double premiumAmount) {

		PremiumCalculator habitsPremiumCalculator = (person, criteria, premiumAmt) -> {

			if (criteria.equalsIgnoreCase(premiumConstants.CALCULATE_HABITS)) {

				double habitPercentage = 0;
				
				if (person.getHabits().getDailyExercise()) {
					habitPercentage -= premiumConstants.HABITS_PREMIUM;
				}

				if (person.getHabits().getSmoking()) {
					habitPercentage += premiumConstants.HABITS_PREMIUM;
				}

				if (person.getHabits().getAlchohol()) {
					habitPercentage += premiumConstants.HABITS_PREMIUM;
				}

				if (person.getHabits().getDrugs()) {
					habitPercentage += premiumConstants.HABITS_PREMIUM;
				}

				premiumAmt += premiumAmt * habitPercentage / 100;

			}
			return premiumAmt;

		};

		return habitsPremiumCalculator.calculate(personObject, premiumConstants.CALCULATE_HABITS, premiumAmount);
	}

}
