package com.emids.test.premiumCalculator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.emids.Entity.Habits;
import com.emids.Entity.Health;
import com.emids.Entity.Person;
import com.emids.EntityImplementation.InsurancePremiumCalculator;
import com.emids.EntityImplementation.premiumConstants;

public class TestInsurancePremiumCalculator {

	private Person person;
	private InsurancePremiumCalculator premiumCalculator = new InsurancePremiumCalculator();

	@Before
	public void setupPerson() {
		person = new Person();

		person.setName("Mr. Gomes");
		person.setHealth(new Health());
		person.setHabits(new Habits());
	}

	@Test
	public void testPerson_checkNull() {
		assertNotNull(person);
	}

	// Test Cases for premium calculation based on Age

	@Test
	public void testPremium_BasedOnAge_AboveEighteen() {
		person.setAge(19);

		double premiumAmount = premiumCalculator.calculateBasedOnAgeCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5500);
	}

	@Test
	public void testPremium_BasedOnAge_AboveTwentyFive() {
		person.setAge(28);

		double premiumAmount = premiumCalculator.calculateBasedOnAgeCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 6050);
	}

	@Test
	public void testPremium_BasedOnAge_AboveThirty() {
		person.setAge(31);

		double premiumAmount = premiumCalculator.calculateBasedOnAgeCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 6655);
	}

	@Test
	public void testPremium_BasedOnAge_AboveThirtyFive() {
		person.setAge(39);

		double premiumAmount = premiumCalculator.calculateBasedOnAgeCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 7321);
	}

	@Test
	public void testPremium_BasedOnAge_AboveForty() {
		person.setAge(42);

		double premiumAmount = premiumCalculator.calculateBasedOnAgeCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 8785);
	}

	// Test Cases for premium calculation based on gender

	@Test
	public void testPremium_BasedOnGenderMale() {
		double premiumAmount = premiumCalculator.calculateBasedOnGenderCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5000);
	}

	@Test
	public void testPremium_BasedOnGenderFemale() {
		person.setGender(premiumConstants.GENDER_FEMALE);

		double premiumAmount = premiumCalculator.calculateBasedOnGenderCriteria(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5000);
	}

	// Test Cases for premium calculation based on Health Condition
	@Test
	public void testPremium_BasedOnHealth_HiperTension() {
		person.getHealth().setHiperTension(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHealthCondition(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5050);
	}

	@Test
	public void testPremium_BasedOnHealth_BP() {
		person.getHealth().setBloodPressure(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHealthCondition(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5050);
	}

	@Test
	public void testPremium_BasedOnHealth_BloodSugar() {
		person.getHealth().setBloodSugar(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHealthCondition(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5050);
	}

	@Test
	public void testPremium_BasedOnHealth_OverWeight() {
		person.getHealth().setOverWeight(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHealthCondition(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5050);
	}

	// Test Cases for premium calculation based on Habits
	@Test
	public void testPremium_BasedOnHabits_Alcholol() {
		person.getHabits().setAlchohol(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHabits(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5150);
	}

	@Test
	public void testPremium_BasedOnHabits_Excercise() {
		person.getHabits().setDailyExercise(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHabits(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 4850);
	}

	@Test
	public void testPremium_BasedOnHabits_Drugs() {
		person.getHabits().setDrugs(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHabits(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5150);
	}

	@Test
	public void testPremium_BasedOnHabits_Smoking() {
		person.getHabits().setSmoking(Boolean.TRUE);

		double premiumAmount = premiumCalculator.calculateBasedOnHabits(person, premiumConstants.BASE_PREMIUM);
		assertTrue(Math.round(premiumAmount) == 5150);
	}

	// Test Cases for premium calculation based on All criterias
	@Test
	public void testPremium_checkTotalPremium() {

		person.setGender(premiumConstants.GENDER_MALE);
		person.setAge(34);

		person.getHealth().setHiperTension(Boolean.FALSE);
		person.getHealth().setBloodPressure(Boolean.FALSE);
		person.getHealth().setBloodSugar(Boolean.FALSE);
		person.getHealth().setOverWeight(Boolean.TRUE);

		person.getHabits().setAlchohol(Boolean.TRUE);
		person.getHabits().setDailyExercise(Boolean.TRUE);
		person.getHabits().setDrugs(Boolean.FALSE);
		person.getHabits().setSmoking(Boolean.FALSE);

		double premiumAmount = premiumCalculator.calculatePremiumPercentage(person);
		assertTrue(premiumAmount == 6856);
	}

}
